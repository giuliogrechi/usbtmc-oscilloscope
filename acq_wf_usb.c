/**
 *   ##############################################
 *   #              Giulio Grechi                 #
 *   #   Corso di Sistemi di acquisizione dati    #
 *   #                                            #
 *   # richiesta e visualizzazione di una forma   #
 *   #    d'onda tramite un oscilloscopio usb     #
 *   #                                            #
 *   ##############################################
*/


/**
 * Il programma esegue una serie di istruzioni per
 * -identificare il tipo di oscilloscopio
 * -richiedere la dimensione del buffer di dati da leggere
 * -richiedere il sample rate
 * -richiedere la scala verticale
 * -richiedere l'offset impostato
 * -richiedere i dati
 * 
 * usando questi dati effettua una conversione dei dati in volt e utilizza
 * gnuplot per mostrare il grafico di V in funzione di t.
 * 
 */

#include  <stdio.h>
#include  <stdlib.h>
#include  <errno.h>
#include  <string.h>
#include  <unistd.h>
#include  <math.h>
#include  <limits.h>
#include  <sys/types.h>
#include  <sys/stat.h>
#include  <fcntl.h>
#include  <ctype.h>

#define SIZE 1000
#define DEV "/dev/usbtmc0"
#define SAVEFILE "output.o"

#define END_CHARACTER 0xa
#define HEAD_DATA_LEN 22
#define COMMAND_REQUEST_DATA "C1:WF? DAT2"
#define COMMAND_IDENTIFY     "*idn?"
#define COMMAND_REQUEST_SARA "SARA?"
#define COMMAND_REQUEST_OFF  "C1:OFFSET?"
#define COMMAND_REQUEST_VDIV "C1:VOLT_DIV?"
#define COMMAND_REQUEST_SANU "SANU? C1"

/**
 * funzione necessaria per convertire le stringhe di risposta dell'oscilloscopio
 * in numeri. E' necessario che l'oscilloscopio sia impostato per produrre
 * risposte numeriche in notazione esponenziale. Ogni stringa contiene
 * tre numeri: parte intera, parte decimale ed esponente. Può essere 
 * indicato il numero del canale, in tal caso è seguito dal carattere : e tale
 * caso va gestito.
 * Il comando CHDR permette di cambiare il modo di risposta, in particolare
 * se si vuole abilitare o meno l'invio dell'header con CHDR OFF. 
 * Questa funzione dovrebbe funzionare comunque in ogni caso.
 */
double convert_to_number(char *b)
{
    int n_bytes_in_buffer[3];
    int i=0;
    int val=0;
    while (*b != 0) { //fino a che ci sono caratteri disponibili

        if ( (*(b+1) != ':') && 
             (isdigit(*b) || ( (*b=='-'||*b=='+') && isdigit(*(b+1)) ))) {

            // trovato un numero
            val = strtol(b, &b, 10); // converti il numero
            n_bytes_in_buffer[i] = val;
            i=i+1;
        }else{
            //altrimenti scorre di un carattere
            b++;
        }
    }

    double result;
    result = (n_bytes_in_buffer[0] * pow(10,n_bytes_in_buffer[2])+
              n_bytes_in_buffer[1] * pow(10,n_bytes_in_buffer[2]-2));

    return result;
}

int request_data(char *buf, int buf_size, char *device)
{
    char *send_vector_data_command = COMMAND_REQUEST_DATA;
    int fd,n;
    fd = open(device,O_RDWR);
    if(fd == -1){

        printf("errore nell'apertura del device %s\n",device);
        return -1;
    }
  
    n = write(fd,send_vector_data_command,strlen(send_vector_data_command));
    if (n != strlen(send_vector_data_command)){

        printf("errore nell'invio del comando %s al dispositivo\n",
                                                     send_vector_data_command);
        fd = close(fd);
        return -1;
    }
    
    n = read(fd,buf,buf_size);
    if (n>0 && n<buf_size){

        if(buf[n-1] == END_CHARACTER){

            buf[n-1] = 0;
            n = n-1;
        }else{
            buf[n] = 0;
        }
    }else if(n==buf_size){
        buf[n]=0;
    }else{

        printf("2- numero di byte letti maggiore "\
                                       "della dimensione del buffer [%d]\n",n);
        fd = close(fd);
        return -1;
    }
    
    fd = close(fd);
    if(fd==-1){

        printf("errore nella chiusura del device %s\n",device);
        return -1;
    }
    return 0;
}


int identify_device(char *buf, int buf_size, char *device)
{
    char *identify_command = COMMAND_IDENTIFY;

    int fd;
    fd = open(device,O_RDWR);
    if(fd == -1){

        printf("errore nell'apertura del device %s\n",device);
        return -1;
    }
  
    int n;
    n = write(fd,identify_command,strlen(identify_command));    
    if (n != strlen(identify_command)){

        printf("errore nell'invio del comando %s al dispositivo\n",
                                                             identify_command);
        fd = close(fd);
        return -1;
    }

    n = read(fd,buf,buf_size);
    if (n>0 && n<buf_size){

        if(buf[n-1] == END_CHARACTER){

            buf[n-1] = 0;
            n = n-1;
        }else{
            buf[n] = 0;
        }
    }else if(n==buf_size){

        buf[n-1]=0;
        fd = close(fd);
        return -1;
    }else{

        printf("1- numero di byte letti maggiore della dimensione "\
                                   "del buffer [%d]\nstringa letta: %s",n,buf);
    return -1;
    }
    
    fd = close(fd);
    if(fd==-1){

        printf("errore nella chiusura del device %s\n",device);
        return -1;
    }
    return 0;
}

int request_sample_rate(char *buf, int buf_size, char *device)
{
    char *sample_rate_read = COMMAND_REQUEST_SARA;

    int fd;
    fd = open(device,O_RDWR);
    if(fd == -1){

        printf("errore nell'apertura del device %s\n",device);
        return -1;
    }
  
    int n;
    n = write(fd,sample_rate_read,strlen(sample_rate_read));    
    if (n != strlen(sample_rate_read)){

        printf("errore nell'invio del comando %s "\
                                          "al dispositivo\n",sample_rate_read);
        return -1;
    }
  
    n = read(fd,buf,buf_size);
    if (n>0 && n<buf_size){

        if(buf[n-1] == END_CHARACTER){

            buf[n-1] = 0;
            n = n-1;
        }else{
            buf[n] = 0;
        }
    }else if(n==buf_size){

        buf[n-1]=0;
        return -1;
    }else{

        printf("1- numero di byte letti maggiore della dimensione "\
                                   "del buffer [%d]\nstringa letta: %s",n,buf);
        return -1;
    }
  
    fd = close(fd);
    if(fd<0){

        printf("errore nella chiusura del device %s\n",device);
        return -1;
    }

    int valore = (int) convert_to_number(buf);

    return valore;
}

double request_offset(char *buf, int buf_size, char *device)
{
    char *offset_read = COMMAND_REQUEST_OFF;  

    int fd;
    fd = open(device,O_RDWR);
    if(fd == -1){

        printf("errore nell'apertura del device %s\n",device);
        return -1;
    }
  
    int n;
    n = write(fd,offset_read,strlen(offset_read));    
    if (n != strlen(offset_read)){

        printf("errore nell'invio del comando %s al dispositivo\n",offset_read);
        close(fd);
        return -1;
    }
  
    n = read(fd,buf,buf_size);
    if (n>0 && n<buf_size){

        if(buf[n-1] == END_CHARACTER){

            buf[n-1] = 0;
            n = n-1;
        }else{

            buf[n] = 0;
        }
    }else if(n==buf_size){

        buf[n-1]=0;
        close(fd);
        return -1;
    }else{

        printf("1- numero di byte letti maggiore della dimensione "\
                                    "del buffer [%d]\nstringa letta: %s",n,buf);
        close(fd);
        return -1;
    }
  
    close(fd);
    if(fd<0){

        printf("errore nella chiusura del device %s\n",device);
        return -1;
    }
    double valore = convert_to_number(buf);
    return valore;
}

double request_volt_division(char *buf, int buf_size, char *device)
{
    char *volt_div_read = COMMAND_REQUEST_VDIV;  

    int fd;    
    fd = open(device,O_RDWR);
    if(fd == -1){

        printf("errore nell'apertura del device %s\n",device);
        return -1;
    }
  
    int n;
    n = write(fd,volt_div_read,strlen(volt_div_read));    
    if (n != strlen(volt_div_read)){

        printf("errore nell'invio del comando %s al dispositivo\n",volt_div_read);
        return -1;
    }

    n = read(fd,buf,buf_size);
    if (n>0 && n<buf_size){

        if(buf[n-1] == END_CHARACTER){

            buf[n-1] = 0;
            n=n-1;
        }else{
            buf[n] = 0;
        }
    }else if(n==buf_size){

        buf[n-1]=0;
        return -1;
    }else{

        printf("1- numero di byte letti maggiore della dimensione del buffer [%d]\n"\
                   "stringa letta: %s",n,buf);
        return -1;
    }

    fd = close(fd);
    if(fd<0){

        printf("errore nella chiusura del device %s\n",device);
        return -1;
    }

    double valore = convert_to_number(buf);
    return valore;
}

int request_size_data_buffer(char *buf, int buf_size, char *device)
{
    char *sample_number_read = COMMAND_REQUEST_SANU;

    int fd;
    fd = open(device,O_RDWR);
    if(fd == -1){

        printf("errore nell'apertura del device %s\n",device);
        return -1;
    }
  
    int n;
    n = write(fd,sample_number_read,strlen(sample_number_read));    
    if (n != strlen(sample_number_read)){

        printf("errore nell'invio del comando %s al dispositivo\n",sample_number_read);
        return -1;
    }
    
    n = read(fd,buf,buf_size);
    if (n>0 && n<buf_size){

        if(buf[n-1] == END_CHARACTER){

            buf[n-1] = 0;
            n=n-1;
        }else{
            buf[n] = 0;
        }
    }else if(n==buf_size){

        buf[n-1]=0;
        return -1;
    }else{

        printf("1- numero di byte letti maggiore della dimensione del buffer [%d]\n"\
                   "stringa letta: %s",n,buf);
        return -1;
    }
    fd = close(fd);
    if(fd<0){

        printf("errore nella chiusura del device %s\n",device);
        return -1;
    }
    int valore = (int) convert_to_number(buf);
    return valore;
}

int main()
{
    /*buffer per le risposte*/
    char data[SIZE];
    
    /*identificazione del device*/
    int status;
    status = identify_device(data,SIZE,DEV);
    if(status<0){
        printf("errore nell'identificazione del dispositivo\n");
    }
    else{
        printf("dispositivo:\n%s\n",data);
    }
    
    /*richiesta della dimensione del buffer dei dati*/
    int n_bytes_to_read = request_size_data_buffer(data,SIZE,DEV);
    if (n_bytes_to_read == -1){

        printf("errore nella lettura della dimensione del buffer\n");
        printf("Stringa letta dal device: %s\n",data);
        return -1;
    }
    else{

        n_bytes_to_read += HEAD_DATA_LEN;
        printf("Numero di byte da leggere: %d\n",n_bytes_to_read);
    }

    /*richiesta del sample rate e calcolo cadenza*/
    int sample_rate = request_sample_rate(data,SIZE,DEV);
    double cadence = 1.0/((double) sample_rate);
    if (sample_rate == -1){

        printf("errore nella lettura del sample rate\n");
        printf("Stringa letta dal device: %s\n",data);
        return -1;
    }
    else{

        printf("Sample rate: %d Sample/s\n",sample_rate);
        printf("cadenza: %.10lf s\n",cadence);
    }

    /*richiesta volt per divisione*/
    double v_division = request_volt_division(data,SIZE,DEV);
    if (v_division == -1){

        printf("errore nella lettura dei volt per divisione\n");
        printf("Stringa letta dal device: %s\n",data);
        return -1;
    }
    else{
        printf("Volt per divisione: %f\n",v_division);
    }
    
    /*richiesta offset*/
    double v_offset = request_offset(data,SIZE,DEV);
    if (v_offset == -1){

        printf("errore nella lettura dell'offset\n'");
        printf("Stringa letta dal device: %s\n",data);
        return -1;
    }
    else{
        printf("offset: %f\n",v_offset);
    }

    /*richiesta vettore di dati*/
    char *vector_data =  malloc(n_bytes_to_read);
    request_data(vector_data,n_bytes_to_read,DEV);

    int i;
    printf("intestazione dati inviati:\n");
    for(i=0;i<HEAD_DATA_LEN;i++){
        printf("%c",vector_data[i]);
    }
    printf("\n");

    /*salvataggio e visualizzazione dei dati */
    FILE * gnuplotPipe = popen ("gnuplot -persistent", "w");
    fprintf(gnuplotPipe, "set xlabel 'tempo [s]' \n");
    fprintf(gnuplotPipe, "set ylabel 'tensione [V]' \n");
    fprintf(gnuplotPipe, "plot '-'\n");
    
    FILE *fdata;
    fdata = fopen(SAVEFILE, "w");
    double voltage_temp;
    double time_temp;
    time_temp = 0;

    /**
     * la funzione per convertire da codice a tensione è da rivedere.
     * 
     * il datasheet prevede:
     * 
     * voltage value (V) = code value *(vdiv /25) - voffset.
     * Note: If the decimal is greater than “127”, it should minus 255. Then the
     * value is code value
     * 
     * il valore assegnato al tempo è riferito al primo punto campionato
     */
    for(i=HEAD_DATA_LEN; i<n_bytes_to_read;i++){

        if((int)vector_data[i]>127){
            voltage_temp = (v_division/25) * ((int)vector_data[i]-255) - v_offset;
        }else{
            voltage_temp = (v_division/25) *  (int)vector_data[i]      - v_offset;
        }
        time_temp += cadence;
        fprintf(fdata,"%.10lf %d %lf\n",time_temp, vector_data[i],voltage_temp);
        fprintf(gnuplotPipe, "%e %e\n", time_temp, voltage_temp);
    }  

    fprintf(gnuplotPipe, "e\n");
    fflush(gnuplotPipe);
    pclose(gnuplotPipe);
    
    fclose(fdata);
    free(vector_data);

    return 0;
}